<?php

namespace App\Controller\User;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Email\Mailer;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\TokenGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SendConfirmationTokenToEmailAction
{
    public function __invoke(User $data,
                             UserRepository $userRepository,
                             LoggerInterface $logger,
                             Mailer $mailer,
                             ValidatorInterface $validator,
                             TokenGenerator $tokenGenerator,
                             EntityManagerInterface $entityManager)
    {
        $validator->validate($data);

        $logger->debug('Fetching user by email');

        $user = $userRepository->findOneBy(
            ['email' => $data->getEmail()]
        );

        // User was NOT found by confirmation token
        if (!$user) {
            $logger->debug('User by email not found');
            throw new NotFoundHttpException('Not found');
        }
        $logger->debug('Send confirmation email for User #' . $user->getId());
        if(!$user->getConfirmationToken()){
            $user->setConfirmationToken($tokenGenerator->getRandomSecureToken());
            $entityManager->flush();
        }

        if (!$user->getEnabled()) {
            $mailer->sendConfirmationEmail($user);
        }

        return new JsonResponse(['send' => true]);
    }
}
