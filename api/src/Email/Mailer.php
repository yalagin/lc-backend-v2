<?php

namespace App\Email;

use App\Entity\CreativeWork;
use App\Entity\Organization;
use App\Entity\OrganizationUnconfirmedMembers;
use App\Entity\Project;
use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class Mailer
{

    private MailerInterface $mailer;

    private $from ;
    private $bcc ;

    public function __construct(
        MailerInterface $mailer
    )
    {
        $this->mailer = $mailer;
        $this->from = $_ENV['EMAIL_FROM'];
        $this->bcc = $_ENV['EMAIL_BCC'];
    }

    public function sendConfirmationEmail(User $user)
    {
        $email = $this->getEmailTemplateAndSetFromAndTo($user)
            ->subject('Please confirm you address')

            // path of the Twig template to render
            ->htmlTemplate('emails/email-confirmation.html.twig')

            // pass variables (name => value) to the template
            ->context([
                'expiration_date' => new \DateTime('+7 days'),
                'user'=> $user,
                'url' => $_ENV['REACT_APP_API_ENTRYPOINT']
            ]);

        $this->mailer->send($email);
    }

    public function sendResetPasswordEmail(User $user)
    {
        $email = $this->getEmailTemplateAndSetFromAndTo($user)
            ->subject('Reset your password')

            // path of the Twig template to render
            ->htmlTemplate('emails/reset-password.html.twig')

            // pass variables (name => value) to the template
            ->context([
                'expiration_date' => new \DateTime('+7 days'),
                'user'=> $user,
                'reset_password_url' => $_ENV['REACT_APP_FRONTEND_ENTRYPOINT'] . '/set-password',
                'url' => $_ENV['REACT_APP_API_ENTRYPOINT']
            ]);

        $this->mailer->send($email);
    }

    public function sendTestEmail($mailAddress)
    {
        $TemplatedEmail  = new TemplatedEmail();
        $TemplatedEmail->from(new Address($this->from))
            ->to(new Address($mailAddress, $mailAddress))
            ->bcc($this->bcc);

        $email = $TemplatedEmail
            ->subject('Test mail')

            // path of the Twig template to render
            ->htmlTemplate('emails/test.html.twig')

            // pass variables (name => value) to the template
            ->context([
                'url' => $_ENV['REACT_APP_API_ENTRYPOINT'],
            ]);
        ;

        $this->mailer->send($email);

    }

    /**
     * @param User $user
     * @return TemplatedEmail
     */
    public function getEmailTemplateAndSetFromAndTo(User $user): TemplatedEmail
    {
        return (new TemplatedEmail())
            ->from(new Address($this->from))
            ->to(new Address($user->getEmail(), $user->getUsername()))
            ->bcc($this->bcc)
            ;
    }
}
