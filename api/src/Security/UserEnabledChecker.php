<?php

namespace App\Security;

use App\Entity\User;
use App\Exception\DisabledException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserEnabledChecker implements UserCheckerInterface
{

    /**
     * Checks the user account before authentication.
     * @throws AccountStatusException
     */
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            return;
        }
        if($user->isAdmin()){
            return;
        }

        if (!$user->getEnabled()) {
            throw new DisabledException('Mohon konfirmasi email');
        }


        if (!$user->getPerson() ) {
            throw new DisabledException('Harap lengkapi profil Anda untuk membantu tim mengenal Anda lebih baik');
        }

        if (!$user->getPerson()->getPhoneNumber() ) {
            throw new DisabledException('Harap tambahkan nomor telepon');
        }
    }

    /**
     * Checks the user account after authentication.
     * @throws AccountStatusException
     */
    public function checkPostAuth(UserInterface $user)
    {

    }
}
