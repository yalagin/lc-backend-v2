<?php


namespace App\Exception;

use Throwable;

class UnauthorizedRoleException extends \Exception
{
    public function __construct(
        string $message = 'Tag did not match the role, please select the role first',
        int $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
