<?php


namespace App\Exception;


use Throwable;

class InvalidCreativeWorkException extends \Exception
{
    public function __construct(
        string $message = 'Invalid submission of creative work',
        int $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
