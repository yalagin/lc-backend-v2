<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * The most generic type of item.
 *
 * @see http://schema.org/Thing Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(iri="http://schema.org/Thing")
 */
class DesignTeespring
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $title40;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description200;

    /**
     * @ORM\OneToOne(targetEntity=Design::class, mappedBy="designTeespring", cascade={"persist", "remove"})
     */
    private ?Design $design;

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setTitle40(?string $title40): void
    {
        $this->title40 = $title40;
    }

    public function getTitle40(): ?string
    {
        return $this->title40;
    }

    public function setDescription200(?string $description200): void
    {
        $this->description200 = $description200;
    }

    public function getDescription200(): ?string
    {
        return $this->description200;
    }

    public function getDesign(): ?Design
    {
        return $this->design;
    }

    public function setDesign(?Design $design): self
    {
        // unset the owning side of the relation if necessary
        if ($design === null && $this->design !== null) {
            $this->design->setDesignTeespring(null);
        }

        // set the owning side of the relation if necessary
        if ($design !== null && $design->getDesignTeespring() !== $this) {
            $design->setDesignTeespring($this);
        }

        $this->design = $design;

        return $this;
    }
}
