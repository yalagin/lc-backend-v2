<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * The most generic type of item.
 *
 * @see http://schema.org/Thing Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(iri="http://schema.org/Thing")
 */
class DesignTeepublic
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $title50;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description200;

    /**
     * @var Collection<Tag>|null
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag")
     * @ORM\JoinTable(inverseJoinColumns={@ORM\JoinColumn(unique=true)})
     */
    private $tags;

    /**
     * @ORM\OneToOne(targetEntity=Design::class, mappedBy="designTeepublic", cascade={"persist", "remove"})
     */
    private ?Design $design;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setTitle50(?string $title50): void
    {
        $this->title50 = $title50;
    }

    public function getTitle50(): ?string
    {
        return $this->title50;
    }

    public function setDescription200(?string $description200): void
    {
        $this->description200 = $description200;
    }

    public function getDescription200(): ?string
    {
        return $this->description200;
    }

    /**
     * @param Tag $tag
     */
    public function addTag($tag): void
    {
        $this->tags[] = $tag;
    }

    /**
     * @param Tag $tag
     */
    public function removeTag($tag): void
    {
        $this->tags->removeElement($tag);
    }

    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function getDesign(): ?Design
    {
        return $this->design;
    }

    public function setDesign(?Design $design): self
    {
        // unset the owning side of the relation if necessary
        if ($design === null && $this->design !== null) {
            $this->design->setDesignTeepublic(null);
        }

        // set the owning side of the relation if necessary
        if ($design !== null && $design->getDesignTeepublic() !== $this) {
            $design->setDesignTeepublic($this);
        }

        $this->design = $design;

        return $this;
    }
}
