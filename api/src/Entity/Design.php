<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * The most generic Design
 *
 * @see http://schema.org/Thing Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(iri="http://schema.org/Thing")
 */
class Design
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @Assert\Uuid
     */
    private $id;

    /**
     * @var MediaObject|null An image of the item. This can be a \[\[URL\]\] or a fully described \[\[ImageObject\]\].
     *
     * @ORM\OneToOne(targetEntity="App\Entity\MediaObject")
     * @ORM\JoinColumn(nullable=false)
     * @ApiProperty(iri="http://schema.org/image")
     */
    private $image;

    /**
     * @var Project|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Project")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;
    /**
     * @var Collection<Tag>|null
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag")
     * @ORM\JoinTable(inverseJoinColumns={@ORM\JoinColumn(unique=true)})
     */
    private $tags;

    /**
     * @var Collection<Bulletpoints>|null
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Bulletpoints")
     * @ORM\JoinTable(inverseJoinColumns={@ORM\JoinColumn(unique=true)})
     */
    private $bulletpoints;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $title26;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $title40;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $title50;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $title60;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $title100;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description140;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description200;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description250;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description2000;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $brand;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $fitMan;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $fitWoman;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $fitYouth;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool|null $isEdit;

    /**
     * @ORM\OneToOne(targetEntity=DesignDisplate::class, inversedBy="design", cascade={"persist", "remove"})
     */
    private DesignDisplate|null $designDisplate;

    /**
     * @ORM\OneToOne(targetEntity=DesignMBA::class, inversedBy="design", cascade={"persist", "remove"})
     */
    private DesignMba|null $designMba;

    /**
     * @ORM\OneToOne(targetEntity=DesignPrintful::class, inversedBy="design", cascade={"persist", "remove"})
     */
    private DesignPrintful|null $designPrintful;

    /**
     * @ORM\OneToOne(targetEntity=DesignRedBubble::class, inversedBy="design", cascade={"persist", "remove"})
     */
    private DesignRedBubble|null $designRedBubble;

    /**
     * @ORM\OneToOne(targetEntity=DesignShirtee::class, inversedBy="design", cascade={"persist", "remove"})
     */
    private DesignShirtee|null $designShirtee;

    /**
     * @ORM\OneToOne(targetEntity=DesignSociety6::class, inversedBy="design", cascade={"persist", "remove"})
     */
    private DesignSociety6|null $designSociety6;

    /**
     * @ORM\OneToOne(targetEntity=DesignSpreadshirtCom::class, inversedBy="design", cascade={"persist", "remove"})
     */
    private DesignSpreadshirtCom|null $designSpreadshirtCom;

    /**
     * @ORM\OneToOne(targetEntity=DesignSpreadshirtEu::class, inversedBy="design", cascade={"persist", "remove"})
     */
    private ?DesignSpreadshirtEu $designSpreadshirtEu;

    /**
     * @ORM\OneToOne(targetEntity=DesignTeepublic::class, inversedBy="design", cascade={"persist", "remove"})
     */
    private ?DesignTeepublic $designTeepublic;

    /**
     * @ORM\OneToOne(targetEntity=DesignTeespring::class, inversedBy="design", cascade={"persist", "remove"})
     */
    private ?DesignTeespring $designTeespring;

    /**
     * @ORM\OneToOne(targetEntity=DesignZazzle::class, inversedBy="design", cascade={"persist", "remove"})
     */
    private ?DesignZazzle $designZazzle;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->bulletpoints = new ArrayCollection();
        $this->isEdit = true;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param MediaObject|null $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }

    /**
     * @return MediaObject|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param Project|null $project
     */
    public function setProject($project): void
    {
        $this->project = $project;
    }

    /**
     * @return Project|null
     */
    public function getProject()
    {
        return $this->project;
    }

    public function addTag(Tag $tag): void
    {
        $this->tags[] = $tag;
    }

    public function removeTag(Tag $tag): void
    {
        $this->tags->removeElement($tag);
    }

    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addBulletpoint(Bulletpoints $bulletpoint): void
    {
        $this->bulletpoints[] = $bulletpoint;
    }

    public function removeBulletpoint(Bulletpoints $bulletpoint): void
    {
        $this->bulletpoints->removeElement($bulletpoint);
    }

    public function getBulletpoints(): Collection
    {
        return $this->bulletpoints;
    }

    public function setTitle26(?string $title26): void
    {
        $this->title26 = $title26;
    }

    public function getTitle26(): ?string
    {
        return $this->title26;
    }

    public function setTitle40(?string $title40): void
    {
        $this->title40 = $title40;
    }

    public function getTitle40(): ?string
    {
        return $this->title40;
    }

    public function setTitle50(?string $title50): void
    {
        $this->title50 = $title50;
    }

    public function getTitle50(): ?string
    {
        return $this->title50;
    }

    public function setTitle60(?string $title60): void
    {
        $this->title60 = $title60;
    }

    public function getTitle60(): ?string
    {
        return $this->title60;
    }

    public function setTitle100(?string $title100): void
    {
        $this->title100 = $title100;
    }

    public function getTitle100(): ?string
    {
        return $this->title100;
    }

    public function setDescription140(?string $description140): void
    {
        $this->description140 = $description140;
    }

    public function getDescription140(): ?string
    {
        return $this->description140;
    }

    public function setDescription200(?string $description200): void
    {
        $this->description200 = $description200;
    }

    public function getDescription200(): ?string
    {
        return $this->description200;
    }

    public function setDescription250(?string $description250): void
    {
        $this->description250 = $description250;
    }

    public function getDescription250(): ?string
    {
        return $this->description250;
    }

    public function setDescription2000(?string $description2000): void
    {
        $this->description2000 = $description2000;
    }

    public function getDescription2000(): ?string
    {
        return $this->description2000;
    }

    public function setBrand(?string $brand): void
    {
        $this->brand = $brand;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setFitMan(?bool $fitMan): void
    {
        $this->fitMan = $fitMan;
    }

    public function getFitMan(): ?bool
    {
        return $this->fitMan;
    }

    public function setFitWoman(?bool $fitWoman): void
    {
        $this->fitWoman = $fitWoman;
    }

    public function getFitWoman(): ?bool
    {
        return $this->fitWoman;
    }

    public function setFitYouth(?bool $fitYouth): void
    {
        $this->fitYouth = $fitYouth;
    }

    public function getFitYouth(): ?bool
    {
        return $this->fitYouth;
    }

    public function getIsEdit(): ?bool
    {
        return $this->isEdit;
    }

    public function setIsEdit(bool $isEdit): self
    {
        $this->isEdit = $isEdit;

        return $this;
    }

    public function getDesignDisplate(): ?DesignDisplate
    {
        return $this->designDisplate;
    }

    public function setDesignDisplate(?DesignDisplate $designDisplate): self
    {
        $this->designDisplate = $designDisplate;

        return $this;
    }

    public function getDesignMba(): ?DesignMba
    {
        return $this->designMba;
    }

    public function setDesignMba(?DesignMba $designMba): self
    {
        $this->designMba = $designMba;

        return $this;
    }

    public function getDesignPrintful(): ?DesignPrintful
    {
        return $this->designPrintful;
    }

    public function setDesignPrintful(?DesignPrintful $designPrintful): self
    {
        $this->designPrintful = $designPrintful;

        return $this;
    }

    public function getDesignRedBubble(): ?DesignRedBubble
    {
        return $this->designRedBubble;
    }

    public function setDesignRedBubble(?DesignRedBubble $designRedBubble): self
    {
        $this->designRedBubble = $designRedBubble;

        return $this;
    }

    public function getDesignShirtee(): ?DesignShirtee
    {
        return $this->designShirtee;
    }

    public function setDesignShirtee(?DesignShirtee $designShirtee): self
    {
        $this->designShirtee = $designShirtee;

        return $this;
    }

    public function getDesignSociety6(): ?DesignSociety6
    {
        return $this->designSociety6;
    }

    public function setDesignSociety6(?DesignSociety6 $designSociety6): self
    {
        $this->designSociety6 = $designSociety6;

        return $this;
    }

    public function getDesignSpreadshirtCom(): ?DesignSpreadshirtCom
    {
        return $this->designSpreadshirtCom;
    }

    public function setDesignSpreadshirtCom(?DesignSpreadshirtCom $designSpreadshirtCom): self
    {
        $this->designSpreadshirtCom = $designSpreadshirtCom;

        return $this;
    }

    public function getDesignSpreadshirtEu(): ?DesignSpreadshirtEu
    {
        return $this->designSpreadshirtEu;
    }

    public function setDesignSpreadshirtEu(?DesignSpreadshirtEu $designSpreadshirtEu): self
    {
        $this->designSpreadshirtEu = $designSpreadshirtEu;

        return $this;
    }

    public function getDesignTeepublic(): ?DesignTeepublic
    {
        return $this->designTeepublic;
    }

    public function setDesignTeepublic(?DesignTeepublic $designTeepublic): self
    {
        $this->designTeepublic = $designTeepublic;

        return $this;
    }

    public function getDesignTeespring(): ?DesignTeespring
    {
        return $this->designTeespring;
    }

    public function setDesignTeespring(?DesignTeespring $designTeespring): self
    {
        $this->designTeespring = $designTeespring;

        return $this;
    }

    public function getDesignZazzle(): ?DesignZazzle
    {
        return $this->designZazzle;
    }

    public function setDesignZazzle(?DesignZazzle $designZazzle): self
    {
        $this->designZazzle = $designZazzle;

        return $this;
    }
}
