<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Uid\Uuid;

/**
 * The most generic type of item.
 *
 * @see http://schema.org/Thing Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(iri="http://schema.org/Thing")
 */
class Project extends AbstractDate
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="uuid")
     * @Assert\Uuid
     */
    private $id;

    /**
     * @var string|null the name of the item
     *
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/name")
     */
    private $name;

    /**
     * @var string|null a description of the item
     *
     * @ORM\Column(type="text", nullable=true)
     * @ApiProperty(iri="http://schema.org/description")
     */
    private $description;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $founder;

    /**
     * @var Collection<User>|null
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User")
     * @ORM\JoinTable(inverseJoinColumns={@ORM\JoinColumn(unique=true)})
     */
    private $collaborates;

    /**
     * @var Collection<Invitations>|null
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Invitations")
     * @ORM\JoinTable(inverseJoinColumns={@ORM\JoinColumn(unique=true)})
     */
    private $invitations;

    public function __construct(User $user,string $name)
    {
        $this->collaborates = new ArrayCollection();
        $this->invitations = new ArrayCollection();
        $this->founder = $user;
        $this->name =$name;
        $this->id = Uuid::v4();
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param User|null $founder
     */
    public function setFounder($founder): void
    {
        $this->founder = $founder;
    }

    /**
     * @return User|null
     */
    public function getFounder()
    {
        return $this->founder;
    }

    /**
     * @param User $collaborate
     */
    public function addCollaborate($collaborate): void
    {
        $this->collaborates[] = $collaborate;
    }

    /**
     * @param User $collaborate
     */
    public function removeCollaborate($collaborate): void
    {
        $this->collaborates->removeElement($collaborate);
    }

    public function getCollaborates(): Collection
    {
        return $this->collaborates;
    }

    public function addInvitation(Invitations $invitation): void
    {
        $this->invitations[] = $invitation;
    }

    public function removeInvitation(Invitations $invitation): void
    {
        $this->invitations->removeElement($invitation);
    }

    public function getInvitations(): Collection
    {
        return $this->invitations;
    }
}
