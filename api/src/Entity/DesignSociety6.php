<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * The most generic type of item.
 *
 * @see http://schema.org/Thing Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(iri="http://schema.org/Thing")
 */
class DesignSociety6
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $title100;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description250;

    /**
     * @var Collection<Tag>|null
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag")
     * @ORM\JoinTable(inverseJoinColumns={@ORM\JoinColumn(unique=true)})
     */
    private $tags;

    /**
     * @ORM\OneToOne(targetEntity=Design::class, mappedBy="designSociety6", cascade={"persist", "remove"})
     */
    private ?Design $design;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setTitle100(?string $title100): void
    {
        $this->title100 = $title100;
    }

    public function getTitle100(): ?string
    {
        return $this->title100;
    }

    public function setDescription250(?string $description250): void
    {
        $this->description250 = $description250;
    }

    public function getDescription250(): ?string
    {
        return $this->description250;
    }

    /**
     * @param Tag $tag
     */
    public function addTag($tag): void
    {
        $this->tags[] = $tag;
    }

    /**
     * @param Tag $tag
     */
    public function removeTag($tag): void
    {
        $this->tags->removeElement($tag);
    }

    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function getDesign(): ?Design
    {
        return $this->design;
    }

    public function setDesign(?Design $design): self
    {
        // unset the owning side of the relation if necessary
        if ($design === null && $this->design !== null) {
            $this->design->setDesignSociety6(null);
        }

        // set the owning side of the relation if necessary
        if ($design !== null && $design->getDesignSociety6() !== $this) {
            $design->setDesignSociety6($this);
        }

        $this->design = $design;

        return $this;
    }
}
