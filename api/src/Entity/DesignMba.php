<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * The most generic type of item.
 *
 * @see http://schema.org/Thing Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(iri="http://schema.org/Thing")
 */
class DesignMba
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $brand;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $title60;

    /**
     * @var Collection<Bulletpoints>|null
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Bulletpoints")
     * @ORM\JoinTable(inverseJoinColumns={@ORM\JoinColumn(unique=true)})
     */
    private $bulletpoints;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description2000;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $fitMan;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $fitWoman;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $fitYouth;

    /**
     * @ORM\OneToOne(targetEntity=Design::class, mappedBy="designMba", cascade={"persist", "remove"})
     */
    private Design|null $design;

    public function __construct()
    {
        $this->bulletpoints = new ArrayCollection();
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setBrand(?string $brand): void
    {
        $this->brand = $brand;
    }

    public function getBrand(): ?string
    {
        return $this->brand;
    }

    public function setTitle60(?string $title60): void
    {
        $this->title60 = $title60;
    }

    public function getTitle60(): ?string
    {
        return $this->title60;
    }

    /**
     * @param Bulletpoints $bulletpoint
     */
    public function addBulletpoint($bulletpoint): void
    {
        $this->bulletpoints[] = $bulletpoint;
    }

    /**
     * @param Bulletpoints $bulletpoint
     */
    public function removeBulletpoint($bulletpoint): void
    {
        $this->bulletpoints->removeElement($bulletpoint);
    }

    public function getBulletpoints(): Collection
    {
        return $this->bulletpoints;
    }

    public function setDescription2000(?string $description2000): void
    {
        $this->description2000 = $description2000;
    }

    public function getDescription2000(): ?string
    {
        return $this->description2000;
    }

    public function setFitMan(?bool $fitMan): void
    {
        $this->fitMan = $fitMan;
    }

    public function getFitMan(): ?bool
    {
        return $this->fitMan;
    }

    public function setFitWoman(?bool $fitWoman): void
    {
        $this->fitWoman = $fitWoman;
    }

    public function getFitWoman(): ?bool
    {
        return $this->fitWoman;
    }

    public function setFitYouth(?bool $fitYouth): void
    {
        $this->fitYouth = $fitYouth;
    }

    public function getFitYouth(): ?bool
    {
        return $this->fitYouth;
    }

    public function getDesign(): ?Design
    {
        return $this->design;
    }

    public function setDesign(?Design $design): self
    {
        // unset the owning side of the relation if necessary
        if ($design === null && $this->design !== null) {
            $this->design->setDesignMBA(null);
        }

        // set the owning side of the relation if necessary
        if ($design !== null && $design->getDesignMba() !== $this) {
            $design->setDesignMba($this);
        }

        $this->design = $design;

        return $this;
    }
}
