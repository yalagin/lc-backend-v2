<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Types\UuidType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\User\ResetPasswordAction;
use App\Controller\User\EnableUserAction;
use App\Controller\User\SendConfirmationTokenToEmailAction;
use App\Controller\User\SendResetPasswordLinkToEmailAction;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;

/**
 *
 * @ApiResource(
 *     collectionOperations={
 *          "get"= {"security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')"},
 *          "post"={
 *              "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *              "validation_groups"={"Default", "user:items:post","user:write"},
 *              "method"="POST",
 *              "denormalization_context"={"groups"={"user:items:post","user:write"}},
 *          },
 *          "post-confirmaniton-token"={
 *             "path"="/users/confirm",
 *              "method"="POST",
 *              "controller"=EnableUserAction::class,
 *              "openapi_context"={"summary" = "Post confirmation token for enabling user"},
 *             "denormalization_context"={"groups"={"user_confirmation:write"}},
 *             "normalization_context"={"groups"={"user_confirmation:read"}},
 *             "validation_groups"={"user_confirmation:write"}
 *         },
 *         "post-send-token-to-email"={
 *             "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY') ",
 *             "method"="POST",
 *             "path"="/user/send-token-to-email",
 *             "controller"=SendResetPasswordLinkToEmailAction::class,
 *             "openapi_context"={"summary" = "Send reset token to users email for resetting password"},
 *             "denormalization_context"={"groups"={"send_to_email:write"}},
 *             "normalization_context"={"groups"={"send_to_email:read"}},
 *             "validation_groups"={"send_to_email:write"}
 *         },
 *          "post-activation-link-to-email"={
 *             "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY') ",
 *             "method"="POST",
 *             "path"="/user/send-activation-link-to-email",
 *             "controller"=SendConfirmationTokenToEmailAction::class,
 *             "openapi_context"={"summary" = "Send confirmation link to email again"},
 *             "denormalization_context"={"groups"={"send_to_email:write"}},
 *             "normalization_context"={"groups"={"send_to_email:read"}},
 *             "validation_groups"={"send_to_email:write"}
 *         },
 *     },
 *     itemOperations={
 *          "get"= {"security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}},
 *          "patch"={
 *              "security"="is_granted('ROLE_USER') and object == user or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"user:item:patch"},
 *              "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}},
 *              "validation_groups"={"user:item:patch"}
 *          },
 *          "put"={
 *              "security"="is_granted('ROLE_USER') and object == user or is_granted('ROLE_ADMIN')",
 *              "denormalization_context"={"groups"={"user:item:patch"},
 *              "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"}},
 *              "validation_groups"={"user:item:patch"}
 *          },
 *          "delete"={
     *          "security"="is_granted('ROLE_ADMIN')",
     *          "requirements"={"id"="\b[0-9a-f]{8}\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\b[0-9a-f]{12}\b"},
 *          },
 *         "post-reset-password"={
 *             "security"="is_granted('IS_AUTHENTICATED_FULLY') and object == user",
 *             "method"="POST",
 *             "path"="/users/{id}/reset-password",
 *             "controller"=ResetPasswordAction::class,
 *             "denormalization_context"={"groups"={"user:item:put-reset-password"}},
 *             "normalization_context"={"groups"={"user:item:put-reset-password"}},
 *             "validation_groups"={"user:item:put-reset-password"},
 *              "openapi_context"={"summary" = "When user don't forget his password and just resetting it"},
 *         },
 *          "post-reset-password-with-token"={
 *             "security"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')",
 *             "method"="POST",
 *             "path"="/users/{id}/reset-password-with-token",
 *             "controller"=ResetPasswordAction::class,
 *             "denormalization_context"={"groups"={"user:item:put-reset-password-with-token"}},
 *             "normalization_context"={"groups"={"user:item:put-reset-password-with-token"}},
 *             "validation_groups"={"user:item:put-reset-password-with-token"},
 *             "openapi_context"={"summary" = "When user forgets his password and recived confirmation token from email"},
 *         }
 *     },
 *      normalizationContext={"groups"={"user:read"}},
 *      denormalizationContext={"groups"={"user:write"}},
 * )
 * @ApiFilter(PropertyFilter::class)
 * @UniqueEntity(fields={"email"},groups={"user:items:post"})
 * @UniqueEntity(fields={"id"})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository",)
 * @ORM\Table(name="`user`")
 * @ApiFilter(SearchFilter::class,properties={"id"})
 * @ApiFilter(OrderFilter::class)
 * @ORM\HasLifecycleCallbacks
 */
class User implements UserInterface
{
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_USER = 'ROLE_USER';

    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @Groups({"user:read", "user:items:post"})
     * @Assert\Uuid(groups={"user:items:post"})
     * @Assert\NotBlank(groups={"user:items:post"})
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"user:read", "user:items:post", "admin:write","send_to_email:write","send_to_email:read"})
     * @Assert\NotBlank(groups={"Default","user:items:post"})
     * @Assert\Email(groups={"Default","user:items:post","send_to_email:write"})
     * @Assert\Length(min=6, max=255,groups={"Default","user:items:post","send_to_email:write"})
     */
    private string $email;

    /**
     * @var array
     * @ORM\Column(type="json",nullable=true)
     * @Groups({"admin:read"})
     */
    private array $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private string $password;

//    /**
//     * @var string Full name
//     * @ORM\Column(type="string", length=255, unique=true)
//     * @Groups({"Default","user:read", "user:write", "user:item:patch"})
//     * @Assert\NotBlank(groups={"user:items:post","user:item:patch"})
//     * @Assert\Length(min=3, max=255,groups={"user:write", "user:item:patch"})
//     */
//    private string $username;

    /**
     * @SerializedName("password")
     * @Assert\Regex(
     *     pattern="/(?=.*[a-zA-Z])(?=.*[0-9]).{7,}/",
     *     message="Password must be seven characters long and contain at least one digit and letters",
     * )
     * @Assert\NotBlank(groups={"user:items:post"})
     * @Groups({"Default","user:items:post"})
     */
    private  $plainPassword;

//    /**
//     * (choices = {User::ROLE_USER})
//     *
//     * @Groups({"user:write", "user:item:patch","user:items:post"})
//     * @SerializedName("role")
//     *
//     */
//    private $roleAssigner;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $passwordChangeDate;

    /**
     * @Groups({"admin:read"})
     * @ORM\Column(type="boolean")
     */
    private bool $enabled = false;

    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $confirmationToken;

    /**
     * @var string
     * @Groups({"user:item:put-reset-password","user:item:put-reset-password-with-token"})
     * @Assert\NotBlank(groups={"user:item:put-reset-password","user:item:put-reset-password-with-token"})
     * @Assert\Regex(
     *     pattern="/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{7,}/",
     *     message="Password must be seven characters long and contain at least one digit, one upper case letter and one lower case letter",
     *     groups={"user:item:put-reset-password","user:item:put-reset-password-with-token"}
     * )
     */
    private $newPassword;

    /**
     * @var string
     * @Groups({"user:item:put-reset-password","user:item:put-reset-password-with-token"})
     * @Assert\NotBlank(groups={"user:item:put-reset-password","user:item:put-reset-password-with-token"})
     * @Assert\Expression(
     *     "this.getNewPassword() === this.getNewRetypedPassword()",
     *     message="Passwords does not match",
     *     groups={"user:item:put-reset-password","user:item:put-reset-password-with-token"}
     * )
     */
    private $newRetypedPassword;

    /**
     * @var string
     * @Groups({"Default","user:item:put-reset-password"})
     * @Assert\NotBlank(groups={"user:item:put-reset-password"})
     * @UserPassword(groups={"user:item:put-reset-password"})
     */
    private $oldPassword;


    /**
     * @ORM\Column(type="string", length=40, nullable=true)
     */
    private $passwordResetToken;

    /**
     * @var string
     * @Groups({"Default","user:item:put-reset-password-with-token"})
     * @Assert\Expression(
     *     "this.getPasswordResetToken() === this.getPasswordResetTokenFromUser()",
     *     message="Token is invalid",
     *     groups={"Default","user:item:put-reset-password-with-token"}
     * )
     * @SerializedName("token")
     * @Assert\NotBlank(groups={"user:item:put-reset-password-with-token"})
     */
    private $passwordResetTokenFromUser;

    /**
     * @var null|DateTimeInterface
     *
     * @ORM\Column(type="datetime",nullable=true, options={"default": "CURRENT_TIMESTAMP"} )
     * @ApiProperty(iri="http://schema.org/DateTime")
     */
    private  $passwordResetTokenTimeStamp;


    /**
     * @var null|DateTimeInterface
     *
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     * @ApiProperty(iri="http://schema.org/DateTime")
     * @Groups({"read","admin:read"})
     */
    protected  $createdAt;

    /**
     * @var null|DateTimeInterface
     *
     * @ORM\Column(type="datetime",nullable=true, options={"default": "CURRENT_TIMESTAMP"} )
     * @ApiProperty(iri="http://schema.org/DateTime")
     * @Groups({"read","admin:read"})
     */
    protected  $updatedAt;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"user_confirmation:write"})
     * @Assert\Length(min=30, max=30,groups={"user_confirmation:write"})
     * @Groups({"user_confirmation:read", "user_confirmation:write"})
     * @ApiProperty()
     * @SerializedName("confirmationToken")
     */
    public $checkConfirmationToken;

    /**
     * @return string
     */
    public function getCheckConfirmationToken(): string
    {
        return $this->checkConfirmationToken;
    }

    /**
     * @param string $checkConfirmationToken
     */
    public function setCheckConfirmationToken(string $checkConfirmationToken): void
    {
        $this->checkConfirmationToken = $checkConfirmationToken;
    }

    public function setCreatedAt( DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getCreatedAt():? DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setUpdatedAt( DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getUpdatedAt():? DateTimeInterface
    {
        return $this->updatedAt;
    }



    public function __construct()
    {
        $this->id = Uuid::v4();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

//    public function setUsername(string $username): self
//    {
//        $this->username = $username;
//
//        return $this;
//    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }
//
//    /**
//     * @return string|null
//     */
//    public function getRoleAssigner(): ?string
//    {
//        return $this->roleAssigner;
//    }
//
//    /**
//     * @param string|null $roleAssigner
//     */
//    public function setRoleAssigner(?string $roleAssigner): void
//    {
//        $this->roleAssigner = $roleAssigner;
//    }

    /**
     * @return mixed
     */
    public function getPasswordChangeDate()
    {
        return $this->passwordChangeDate;
    }

    /**
     * @param mixed $passwordChangeDate
     * @return User
     */
    public function setPasswordChangeDate($passwordChangeDate)
    {
        $this->passwordChangeDate = $passwordChangeDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param mixed $enabled
     * @return User
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * @param mixed $confirmationToken
     * @return User
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param mixed $newPassword
     * @return User
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNewRetypedPassword()
    {
        return $this->newRetypedPassword;
    }

    /**
     * @param mixed $newRetypedPassword
     * @return User
     */
    public function setNewRetypedPassword($newRetypedPassword)
    {
        $this->newRetypedPassword = $newRetypedPassword;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * @param mixed $oldPassword
     * @return User
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPasswordResetToken()
    {
        return $this->passwordResetToken;
    }

    /**
     * @param mixed $passwordResetToken
     * @return User
     */
    public function setPasswordResetToken($passwordResetToken)
    {
        $this->passwordResetToken = $passwordResetToken;
        return $this;
    }

    /**
     * @return null| string
     */
    public function getPasswordResetTokenFromUser(): ?string
    {
        return $this->passwordResetTokenFromUser;
    }

    /**
     * @param string $passwordResetTokenFromUser
     * @return User
     */
    public function setPasswordResetTokenFromUser(string $passwordResetTokenFromUser): User
    {
        $this->passwordResetTokenFromUser = $passwordResetTokenFromUser;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getPasswordResetTokenTimeStamp(): ?DateTimeInterface
    {
        return $this->passwordResetTokenTimeStamp;
    }

    /**
     * @param DateTimeInterface|null $passwordResetTokenTimeStamp
     * @return User
     */
    public function setPasswordResetTokenTimeStamp(?DateTimeInterface $passwordResetTokenTimeStamp): User
    {
        $this->passwordResetTokenTimeStamp = $passwordResetTokenTimeStamp;
        return $this;
    }
}
