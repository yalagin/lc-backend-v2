<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * The most generic type of item.
 *
 * @see http://schema.org/Thing Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(iri="http://schema.org/Thing")
 */
class DesignZazzle
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $title50;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description2000;

    /**
     * @var Collection<Tag>|null
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag")
     * @ORM\JoinTable(inverseJoinColumns={@ORM\JoinColumn(unique=true)})
     */
    private $tags;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $occasion;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $audience;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $category;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $event;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $recipient;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $store;

    /**
     * @ORM\OneToOne(targetEntity=Design::class, mappedBy="designZazzle", cascade={"persist", "remove"})
     */
    private ?Design $design;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setTitle50(?string $title50): void
    {
        $this->title50 = $title50;
    }

    public function getTitle50(): ?string
    {
        return $this->title50;
    }

    public function setDescription2000(?string $description2000): void
    {
        $this->description2000 = $description2000;
    }

    public function getDescription2000(): ?string
    {
        return $this->description2000;
    }

    /**
     * @param Tag $tag
     */
    public function addTag($tag): void
    {
        $this->tags[] = $tag;
    }

    /**
     * @param Tag $tag
     */
    public function removeTag($tag): void
    {
        $this->tags->removeElement($tag);
    }

    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function setOccasion(?string $occasion): void
    {
        $this->occasion = $occasion;
    }

    public function getOccasion(): ?string
    {
        return $this->occasion;
    }

    public function setAudience(?string $audience): void
    {
        $this->audience = $audience;
    }

    public function getAudience(): ?string
    {
        return $this->audience;
    }

    public function setCategory(?string $category): void
    {
        $this->category = $category;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setEvent(?string $event): void
    {
        $this->event = $event;
    }

    public function getEvent(): ?string
    {
        return $this->event;
    }

    public function setRecipient(?string $recipient): void
    {
        $this->recipient = $recipient;
    }

    public function getRecipient(): ?string
    {
        return $this->recipient;
    }

    public function setStore(?string $store): void
    {
        $this->store = $store;
    }

    public function getStore(): ?string
    {
        return $this->store;
    }

    public function getDesign(): ?Design
    {
        return $this->design;
    }

    public function setDesign(?Design $design): self
    {
        // unset the owning side of the relation if necessary
        if ($design === null && $this->design !== null) {
            $this->design->setDesignZazzle(null);
        }

        // set the owning side of the relation if necessary
        if ($design !== null && $design->getDesignZazzle() !== $this) {
            $design->setDesignZazzle($this);
        }

        $this->design = $design;

        return $this;
    }
}
