<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * The most generic type of item.
 *
 * @see http://schema.org/Thing Documentation on Schema.org
 *
 * @author Maxim Yalagin <yalagin@gmail.com>
 *
 * @ORM\Entity
 * @ApiResource(iri="http://schema.org/Thing")
 */
class DesignDisplate
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @Assert\Uuid
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $title26;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description140;

    /**
     * @var Collection<Tag>|null
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag")
     * @ORM\JoinTable(inverseJoinColumns={@ORM\JoinColumn(unique=true)})
     */
    private $tags;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $collection;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $category;

    /**
     * @ORM\OneToOne(targetEntity=Design::class, mappedBy="designDisplate", cascade={"persist", "remove"})
     */
    private Design|null $design;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setTitle26(?string $title26): void
    {
        $this->title26 = $title26;
    }

    public function getTitle26(): ?string
    {
        return $this->title26;
    }

    public function setDescription140(?string $description140): void
    {
        $this->description140 = $description140;
    }

    public function getDescription140(): ?string
    {
        return $this->description140;
    }

    /**
     * @param Tag $tag
     */
    public function addTag($tag): void
    {
        $this->tags[] = $tag;
    }

    /**
     * @param Tag $tag
     */
    public function removeTag($tag): void
    {
        $this->tags->removeElement($tag);
    }

    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function setCollection(?string $collection): void
    {
        $this->collection = $collection;
    }

    public function getCollection(): ?string
    {
        return $this->collection;
    }

    public function setCategory(?string $category): void
    {
        $this->category = $category;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function getDesign(): ?Design
    {
        return $this->design;
    }

    public function setDesign(?Design $design): self
    {
        // unset the owning side of the relation if necessary
        if ($design === null && $this->design !== null) {
            $this->design->setDesignDisplate(null);
        }

        // set the owning side of the relation if necessary
        if ($design !== null && $design->getDesignDisplate() !== $this) {
            $design->setDesignDisplate($this);
        }

        $this->design = $design;

        return $this;
    }
}
