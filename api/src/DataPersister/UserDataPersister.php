<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Email\Mailer;
use App\Entity\Project;
use App\Entity\User;
use App\Security\TokenGenerator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserDataPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(private EntityManagerInterface $entityManager,
                                private UserPasswordEncoderInterface $userPasswordEncoder,
                                private TokenGenerator $tokenGenerator,
                                private RequestStack $requestStack,
                                private Mailer $mailer){}

    public function supports($data, array $context = []): bool
    {
        return $data instanceof User;
    }

    public function persist($data, array $context = [])
    {
        //saving hashing password here...
        if ($data->getPlainPassword()) {
            $data->setPassword(
                $this->userPasswordEncoder->encodePassword($data, $data->getPlainPassword())
            );
            $data->eraseCredentials();
            // After password change, old tokens are still valid
            $data->setPasswordChangeDate(time());
        }
        // Create confirmation token...
        $data->setConfirmationToken(
            $this->tokenGenerator->getRandomSecureToken()
        );
        $this->entityManager->persist($data);
        $this->entityManager->flush();

        if (
            $data instanceof User && (
                ($context['collection_operation_name'] ?? null) === 'post' ||
                ($context['graphql_operation_name'] ?? null) === 'create'
            )
        ) {
            $project = new Project($data,$data->getUsername());
            $this->entityManager->persist($project);
            $this->entityManager->flush();

            $this->mailer->sendConfirmationEmail($data);
        }

        return $data;
    }

    public function remove($data, array $context = [])
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}
